using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using MattStudley.BackEnd.Models;
using MattStudley.BackEnd.DAL.UnitOfWork;
using MattStudley.BackEnd.Logging;

namespace MattStudley.BackEnd.Controllers
{
    [Route("api/[controller]")]
    public class SkillCategoryController : Controller
    {
        private ISkillsUnitOfWork skillData;
        private ILogSession<SkillCategoryController> logger;

        public SkillCategoryController(ISkillsUnitOfWork skillUnitOfWork, ILogSession<SkillCategoryController> logSession)
        {
            logger = logSession;
            skillData = skillUnitOfWork;
        }

        [HttpGet]
        public IEnumerable<SkillCategory> Get()
        {
            var categories = skillData.SkillCategoryRepository.Get(null,null,"Skills").ToList();
            return categories;
        }

        // POST api/SkillCategory
        [HttpPost]
        public void Post([FromBody]SkillCategory category)
        {
            try
            {
                skillData.SkillCategoryRepository.Insert(category);
                if (!skillData.Save())
                {
                    logger.LogError(1001, null, "Could not insert skill category.");
                    throw new Exception("Could not insert skill category!");
                }
            }
            catch (Exception ex)
            {
                    logger.LogError(1001, ex, "Could not insert skill category.");
                    throw new Exception("Could not insert skill category!");
            }
        }

        [HttpDelete]
        public void Delete(int categoryId)
        {
            try
            {
                skillData.SkillCategoryRepository.Delete(categoryId);
                if (!skillData.Save())
                {
                    logger.LogError(1005, null, "Could not delete skill category.");
                    throw new Exception("Could not delete skill category!");
                }
            }
            catch (Exception ex)
            {
                    logger.LogError(1005, ex, "Could not delete skill category.");
                    throw new Exception("Could not delete skill category!");
            }
        }


    }
}