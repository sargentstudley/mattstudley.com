﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MattStudley.BackEnd.Models;
using MattStudley.BackEnd.DAL.UnitOfWork;
using MattStudley.BackEnd.Logging;

namespace MattStudley.BackEnd.Controllers
{
    [Route("api/[controller]")]
    public class SkillController : Controller
    {
        private ISkillsUnitOfWork skillData;
        private ILogSession<SkillController> logger;

        public SkillController(ISkillsUnitOfWork skillUnitOfWork, ILogSession<SkillController> logSession)
        {
            logger = logSession;
            skillData = skillUnitOfWork;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Skill> Get()
        {
            var skills = skillData.SkillRepository.Get(null,null).ToList();
            return skills;
        }

        // GET api/skill/5
        [HttpGet("{id}")]
        public Skill Get(int id)
        {
            var skill = skillData.SkillRepository.GetByID(id);

            if (skill == null)
            {
                this.NotFound();
            }

            return skill;
        }

        [HttpPut]
        public IActionResult Put([FromBody]Skill skill)
        {
            if (skill.SkillId == 0)
            {
                return BadRequest( new {Message = "SkillId must be populated."});
            }

            try
            {
                skillData.SkillRepository.Update(skill);
                skillData.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                logger.LogError(5003,ex,ex.Message);
                return StatusCode(500);
            }
        }

        // POST api/Skill
        [HttpPost]
        public void Post([FromBody]Skill skill)
        {
            try
            {
                skillData.SkillRepository.Insert(skill);
                if (!skillData.Save())
                {
                    //TODO: Create enums for these event IDs. 
                    logger.LogError(1001, null, "Could not insert skill.");
                    throw new Exception("Could not insert skill!");
                }
            }
            catch (Exception ex)
            {
                    logger.LogError(1001, ex, "Could not insert skill!");
                    throw new Exception("Could not insert skill!");
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                skillData.SkillRepository.Delete(id);
                if (!skillData.Save())
                {
                    logger.LogError(1003, null, "Could not insert skill.");
                    throw new Exception("Could not insert skill!");
                }
            }
            catch (Exception ex)
            {
                    logger.LogError(1003, ex, "Could not insert skill!");
                    throw new Exception("Could not insert skill!");
            }
        }
    }
}
