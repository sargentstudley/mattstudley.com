using MattStudley.BackEnd.Models;
namespace MattStudley.BackEnd.Controllers.Contracts
{
    public class SkillCategoryWithScore : SkillCategory
    {
        public int Score {get;set;}
    }
}