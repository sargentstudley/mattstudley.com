// Karma configuration
// Generated on Sun Nov 20 2016 19:45:00 GMT-0500 (Eastern Standard Time)

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // Define plugins
    plugins: [
      'karma-jasmine',
      'karma-phantomjs-launcher',
      'karma-html-detailed-reporter',
      'karma-chrome-launcher'
    ],


    // list of files / patterns to load in the browser
    files: [
      'node_modules/babel-polyfill/dist/polyfill.js',
      'wwwroot/lib/jquery/dist/jquery.min.js',
      'wwwroot/lib/bootstrap/dist/js/bootstrap.min.js',
      'wwwroot/lib/angular/angular.min.js',
      'wwwroot/lib/angular-mocks/angular-mocks.js',
      'wwwroot/lib/angular-route/angular-route.js',
      'wwwroot/lib/angular-cookies/angular-cookies.js',
      'wwwroot/lib/angular-gravatar/build/angular-gravatar.js',
      'wwwroot/lib/angular-bootstrap/ui-bootstrap.min.js',
      'wwwroot/lib/chart.js/dist/Chart.bundle.min.js',
      'wwwroot/lib/angular-chart.js/dist/angular-chart.min.js',
      'wwwroot/app/app.module.js',
      'wwwroot/app/**/*.service.js',
      'wwwroot/**/*.spec.js',
    ],


    // list of files to exclude
    exclude: [

    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'htmlDetailed'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

    client: {
      captureConsole: true,
    },


    // configure the HTML-Detailed-Reporter to put all results in one file  
    htmlDetailed: {
      splitResults: false
    }
  })
}
