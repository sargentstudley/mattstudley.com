namespace MattStudley.BackEnd.Models
{
    public class Skill
    {
        public int SkillId {get;set;}
        public string Name { get; set; }
        public int Score { get; set; }

        public int SkillCategoryId {get;set;}
        public SkillCategory Category {get;set;}
        public bool TrainingNow {get;set;}
    }
}