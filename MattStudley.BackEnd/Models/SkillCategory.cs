using System.Collections.Generic;

namespace MattStudley.BackEnd.Models
{
    public class SkillCategory
    {
        public int SkillCategoryId {get;set;}
        public string name {get;set;}
        public List<Skill> Skills {get;set;}
    }
}