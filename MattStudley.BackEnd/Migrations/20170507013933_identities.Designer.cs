﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using MattStudley.BackEnd.DAL.Contexts;

namespace MattStudley.BackEnd.Migrations
{
    [DbContext(typeof(MattStudleyContext))]
    [Migration("20170507013933_identities")]
    partial class identities
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("MattStudley.BackEnd.Models.Skill", b =>
                {
                    b.Property<int>("SkillId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("Score");

                    b.Property<int>("SkillCategoryId");

                    b.HasKey("SkillId");

                    b.HasIndex("SkillCategoryId");

                    b.ToTable("Skills");
                });

            modelBuilder.Entity("MattStudley.BackEnd.Models.SkillCategory", b =>
                {
                    b.Property<int>("SkillCategoryId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("name");

                    b.HasKey("SkillCategoryId");

                    b.ToTable("SkillCategorys");
                });

            modelBuilder.Entity("MattStudley.BackEnd.Models.Skill", b =>
                {
                    b.HasOne("MattStudley.BackEnd.Models.SkillCategory", "Category")
                        .WithMany("Skills")
                        .HasForeignKey("SkillCategoryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
