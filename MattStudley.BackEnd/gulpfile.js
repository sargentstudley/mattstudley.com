var gulp = require('gulp');
var gutil = require('gulp-util');
var notify = require('gulp-notify');
var less = require('gulp-less');
var exec = require('child_process').exec;
var Server = require('karma').Server;

// Where do you store your Sass files?
var lessDir = 'wwwroot/assets/less';

// Which directory should LESS compile to?
var targetCSSDir = 'wwwroot/assets/css';

// Compile Sass, autoprefix CSS3,
// and save to target CSS directory
gulp.task('css', function () {
    return gulp.src(lessDir + '/mattstudley.less')
        .pipe(less({ style: 'compressed' }).on('error', gutil.log))
        .pipe(gulp.dest(targetCSSDir))
        .pipe(notify('CSS minified'))
});

//Run the dotnet build...
gulp.task('dotnetbuild', function (cb) {
  exec('dotnet build ./MattStudley.BackEnd.csproj', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

/**
 * Run karma tests
 */
gulp.task('karmaTest', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

// Keep an eye on files for changes...
gulp.task('watch', function () {
    gulp.watch(lessDir + '/*.less', ['css']);
});

// What tasks does running gulp trigger?
gulp.task('default', ['css','dotnetbuild','karmaTest']);