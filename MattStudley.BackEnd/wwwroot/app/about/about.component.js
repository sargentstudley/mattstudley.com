angular.module('mattstudley').component('about', {
    templateUrl: '/app/about/about.html',
    controller: ['$rootScope' , '$scope', function aboutPageController($rootScope, $scope) {
        
        var dateStarted = new Date(1985, 3, 30, 9, 27, 0, 0);
        
        var getTimeInTech = function () {
            var diff = new Date() - dateStarted;

            var years = Math.floor(diff / (1000 * 60 * 60 * 24 * 365));
            diff -= years * (1000 * 60 * 60 * 24 * 365);

            var days = Math.floor(diff / (1000 * 60 * 60 * 24));
            diff -= days * (1000 * 60 * 60 * 24);

            var hours = Math.floor(diff / (1000 * 60 * 60));
            diff -= hours * (1000 * 60 * 60);

            var mins = Math.floor(diff / (1000 * 60));
            diff -= mins * (1000 * 60);

            var seconds = Math.floor(diff / (1000));
            diff -= seconds * (1000);

            return years + " years " +  days + " days, " + hours + " hours, " + mins + " minutes, and " + seconds + " seconds";
        };

        var timeInTech = getTimeInTech();
        $scope.timeInTech = timeInTech;

        $scope.handleMapMouseover = function() {
            $rootScope.$broadcast('unlockbadge','walkabout');
        }

        setInterval(function(){
            $scope.timeInTech = getTimeInTech();
            $scope.$apply();
        },1000);
    }]
});