angular.module('mattstudley')
    .component('frontpage', {
        templateUrl: '/app/frontPage/frontpage.html',
        controller: ['$rootScope', '$scope', 'skillService', function frontPageController($rootScope, $scope, skillService) {

            $scope.skillChartData = [];
            $scope.skillChartLabels = [];

            $scope.badge = function () {
                return $rootScope.lastUnlockedBadge;
            }

            $scope.popoverTemplate = "myPopoverTemplate.html";

            $scope.getUnlockedBadgeCount = function () {
                return $rootScope.unlockedBadgeCount;
            }

            $scope.onTrophyMouseOver = function () {
                $rootScope.$broadcast('unlockbadge', 'firststeps');
            };

            //Loops through a skill category and returns an average of it's contained skills. Returns that number. 
            function countSkills(skillCategory) {
                var sumOfSkills = 0;
                var numOfSkills = 0;
                for (var i = 0; i < skillCategory.skills.length; i++) {
                    var currentSkill = skillCategory.skills[i];
                    sumOfSkills += currentSkill.score;
                    numOfSkills++;
                }

                if (numOfSkills > 0) {
                    return sumOfSkills / numOfSkills;
                }
                else {
                    return 0;
                }

            }

            $scope.skillChartOptions = {
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                                min: 0,
                                max: 5
                            },
                        }
                    ],
                    xAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                                min: 0,
                                max: 5
                            },
                            position: 'top'
                        }
                    ]
                }
            };

            var skillData = {};

            skillService.getSkillCategories().then(function (data) {
                $scope.skillChartLabels = [];
                $scope.skillChartData = [];

                $scope.$apply(function () {
                    var skillSeriesPointArray = [];
                    //Loop through each category. Add the name of the category to the labels. 
                    for (var i = 0; i < data.length; i++) {
                        var currentSkillCategory = data[i];
                        $scope.skillChartLabels.push(currentSkillCategory.name);
                        skillSeriesPointArray.push(countSkills(currentSkillCategory));
                    }

                    $scope.skillChartData.push(skillSeriesPointArray);
                });
            },
                function (errorReason) {
                    console.log('Could not render skill radar chart: ' + errorReason);
                });

            $scope.chartColors = ['#325D88',
                '#93C54B',
                '#29ABE0',
                '#F47C3C',
                '#d9534f'];
        }]
    });