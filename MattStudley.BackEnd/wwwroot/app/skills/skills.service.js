angular.
    module('mattstudley')
    .service('skillService', ['$http', function ($http) {
        var self = this;

        //Init the skill categories. 
        self.skillCategories = {};

        //Gets all skill categories from the backend. Only makes the http call once, then caches the results. 
        self.getSkillCategories = function() {
            return new Promise(function (resolve,reject) {
                if (Object.keys(self.skillCategories).length === 0 && self.skillCategories.constructor === Object) {
                    $http.get('/api/skillcategory').then(function success(response) {
                        //No skill categories cached. We need to go get them. 
                        self.skillCategories = response.data;
                        resolve(self.skillCategories);
                    },
                    function error(response) {
                        console.error("Couldn't load skills from http source:" + response);
                        reject(response);
                    });
                }
                else {
                    //Skill categories are cached. Return them immediately. 
                    resolve(self.skillCategories);
                }

                
            }); 
        };
     }]);
