angular.module('mattstudley')
    .component('skills',{ 
        templateUrl: '/app/skills/skills.html',
        controller: [
        '$scope',
        'skillService',
        function ($scope, skillService) {

            //Get skill categories from the skills service and then populate it into a scope variable. 
            skillService.getSkillCategories().then(function (data) {
                
                $scope.$apply(function() {
                    $scope.skillCategories = data;
                });
            },
                function (errorReason) {
                    console.log('Could not render skill radar chart: ' + errorReason);
                });
            
            $scope.getProgressBarPercent = function(score) {
                return ((score / 5) * 100).toFixed(2);
            };

            $scope.shouldDisplayLearningBar = function(skill) {
                return skill.score < 5 && skill.trainingNow
            }
        }
    ]});