describe("Badge service", function () {

    var badgeServiceUnderTest;
    var $httpBackendMock;

    var mockBadgeResponse = {
        "version": 0.1,
        "badges": [
            {
                "name": "walkabout",
                "displayname": "Walkabout",
                "hint": "Take a walk down Matt's home street.",
                "description": "You checked out Matt's home street on the about page.",
                "icon": "fa-map",
                "iconcolor": "#33cc33",
                "achieved": false
            },
            {
                "name": "firststeps",
                "displayname": "First Steps",
                "hint": "Find your first badge on the home page. It's the first thing you'll see.",
                "description": "You found the first badge on the home page. How many more can you find?",
                "icon": "fa-trophy",
                "iconcolor": "#3399ff",
                "achieved": false
            }
        ]
    };

    beforeEach(function () {
        module('mattstudley');

        inject(['badgeService', '$httpBackend', function (badgeService, $httpBackend) {
            badgeServiceUnderTest = badgeService;
            $httpBackendMock = $httpBackend;
        }
        ]);
    });

    it('should be injected', function () {
        expect(badgeServiceUnderTest).toBeTruthy();
        expect(badgeServiceUnderTest).not.toEqual(null);
    });

    it('should get a list of badges', function (done) {
        //Mock http response:
        $httpBackendMock.whenGET('/assets/js/badges/badges.json')
            .respond(200, mockBadgeResponse);

        badgeServiceUnderTest.getBadges()
            .then(function (badges) {
                expect(badges).toBeTruthy();
                expect(badges).not.toEqual(null);
                expect(badges.length).toEqual(mockBadgeResponse.badges.length);
                done();
            })
            .catch(function (error) {
                fail(error);
                done(); //TODO: Check if this is Unreachable.
            });
        $httpBackendMock.flush();
    });
});

