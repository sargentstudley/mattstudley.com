angular.
    module('mattstudley')
    .component('badgetray',{
        templateUrl: '/app/badges/badgetray.html',
        controller: ['$scope','badgeService',function($scope, badgeService) {
            this.badges = null;
            badgeService.getBadges()
            .then(function(badges){
                $scope.badges = badges;
                $scope.$apply();
             })
            .catch(function(error) {
                console.error("Badgetray couldn't load badges: " + error);
                $scope.badges = {};
             });    
        }]
    });