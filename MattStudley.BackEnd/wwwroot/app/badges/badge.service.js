angular.
    module('mattstudley').
    service('badgeService', ['$http', '$rootScope', '$cookies', '$timeout', function ($http, $rootScope, $cookies, $timeout) {
        //Event listener that saves the badge in cookie.
        var self = this;
        self.badgestore = null;
        $rootScope.unlockedBadgeCount = 0;

        //Helper method to count number of unlocked badges.
        self.getUnlockedBadgeCount = function() {
           return self.badgestore.badges.filter(function (badge) {
                return badge.achieved == true;
            });
        }

        //Define handler for the badge unlocks. 
        self.handleUnlockBadgeEvent = function (event, badgename) {
            //TODO: This code handles events raised by anywhere else in the app when
            //TODO: Fire event that displays the badge unlock component. 
            var matchingBadges = self.badgestore.badges.filter(function (badge) {
                return badge.name == badgename;
            });

            if (matchingBadges.length > 0) {
                var foundBadge = matchingBadges[0];

                //Don't do anything if the badge is already unlocked. 
                if (foundBadge.achieved == true) {
                    return;
                }

                foundBadge.achieved = true;
                $rootScope.lastUnlockedBadge = foundBadge;
                

                $('#badgeIndicator').trigger('show');

                $timeout(function() {
                    $('#badgeIndicator').trigger('hide');
                },7000);

                //Save our progress. 
                $cookies.put('badges', JSON.stringify(self.badgestore));
                $rootScope.unlockedBadgeCount += 1;
                
            }
            else {
                console.error("Could not find badge with badgename of " + badgename);
                return; //Short circuit out. 
            }
        }; //End handleUnlockBadgeEvent

        ///Private methods///

        // Initialize the service, if this is the first time calling it. 
        var initialize = function () {
            return new Promise(function (resolve, reject) {
                console.log("Init starting.");
                //If the user has the badgestore cookie, use that as a source. Otherwise, get the template from Json.
                var jsonCookieString = $cookies.get('badges');

                console.log("Cookies fetched.");

                //Try to parse our string, if it is not blank. 
                if (jsonCookieString != null) {
                    self.badgestore = JSON.parse(jsonCookieString);
                }
                else {
                    self.badgestore == null;
                }

                //If there are no cookies, an HTTP get will populate the badges. New sets of badges will result in a badge reset for now. 
                $http.get('/assets/js/badges/badges.json').then(function success(response) {
                    if (self.badgestore != null) {
                        if (self.badgestore.version < response.data.version) {
                            self.badgestore = version; //TODO: Migration of achievements will happen later.
                        }
                        
                        var badgeCount = self.getUnlockedBadgeCount();
                        $rootScope.unlockedBadgeCount = self.getUnlockedBadgeCount().length;

                        resolve();
                    }
                    else {
                        //Badgestore is null
                        self.badgestore = response.data;
                        
                        $rootScope.unlockedBadgeCount = self.getUnlockedBadgeCount().length;
                        resolve();
                    }
                },
                    function error(response) {
                        console.error("Couldn't load badges from http source:" + response);
                        reject(response);
                    });
            });
        }; //End Initialize

        ///Public Methods///

        // Gets a list of badges in their current state.
        self.getBadges = function () {
            return new Promise(function (resolve, reject) {
                if (self.badgestore != null) {

                    resolve(self.badgestore.badges);
                }
                else {
                    initialize().then(function () {
                        console.log("Init Finished.");
                        resolve(self.badgestore.badges);
                    })
                        .catch(function (error) {
                            console.log("Init Errored: " + error);
                            reject(error);
                        });
                }
            })
        };

        ///Event Handlers///

        //Main event handler when a badge is unlocked. 
        $rootScope.$on('unlockbadge', self.handleUnlockBadgeEvent);
    }]);