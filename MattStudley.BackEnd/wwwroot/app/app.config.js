angular.module('mattstudley').
    config(['$locationProvider', '$routeProvider', '$uibTooltipProvider', function ($locationProvider, $routeProvider, $uibTooltipProvider) {

        $uibTooltipProvider.setTriggers({
            'show': 'hide'
        });

        $locationProvider.hashPrefix('!');
        $routeProvider.
            when('/home', {
                template: '<frontpage></frontpage>'
            }).
            when('/about', {
                template: '<about></about>'
            }).
            when('/skills', {
                template:'<skills></skills>'
            }).
            otherwise('/home');
    }
    ]);