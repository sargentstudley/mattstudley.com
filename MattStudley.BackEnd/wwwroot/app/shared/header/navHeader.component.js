﻿angular.module('mattstudley').component('navheader', {
    templateUrl: '/app/shared/header/navHeader.html',
    controller: ['$rootScope', '$scope',function navHeaderController($rootScope, $scope) {
        $scope.badge = function() {
            return $rootScope.lastUnlockedBadge;
        }
        $scope.popoverTemplate = "myPopoverTemplate.html";
        $scope.getUnlockedBadgeCount = function() {
            return $rootScope.unlockedBadgeCount;
        }
    }]
});