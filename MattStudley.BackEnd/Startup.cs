﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

using MattStudley.BackEnd.Logging;
using MattStudley.BackEnd.DAL.Contexts;
using MattStudley.BackEnd.DAL.Repositories;
using MattStudley.BackEnd.DAL.UnitOfWork;

namespace MattStudley.BackEnd
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            Environment = env;
        }

        public IConfigurationRoot Configuration { get; }
        private IHostingEnvironment Environment { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc()
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            //We need to switch here to change between sqlite for Dev and SqlServer for production. 
            if (Environment.IsProduction())
            {
                services.AddDbContext<MattStudleyContext>(options => options.UseSqlServer(this.GetRDSConnectionString()));
            }
            else
            {
                services.AddDbContext<MattStudleyContext>(options => options.UseSqlite("Filename=./mattstudley2.sqlite"));
            }

            // Configure providers
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped<ISkillsUnitOfWork, SkillsUnitOfWork>();
            services.AddScoped(typeof(ILogSession<>), typeof(LogSession<>));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            loggerFactory.AddDebug();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMvc();

            //Init the database if it is not already up and running. 
            var context = app.ApplicationServices.GetService<MattStudleyContext>();
            context.Database.Migrate();
        }

        private string GetRDSConnectionString()
        {


            string dbname = System.Environment.GetEnvironmentVariable("DB_Database");

            if (string.IsNullOrEmpty(dbname)) return null;

            string username = System.Environment.GetEnvironmentVariable("DB_User");
            string password = System.Environment.GetEnvironmentVariable("DB_Password");
            string hostname = System.Environment.GetEnvironmentVariable("DB_Server");
            string port = System.Environment.GetEnvironmentVariable("DB_Port");

            return "Data Source=" + hostname + ";Initial Catalog=" + dbname + ";User ID=" + username + ";Password=" + password + ";";
        }
    }
}
