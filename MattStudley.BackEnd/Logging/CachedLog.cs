using System;
using Microsoft.Extensions.Logging;

namespace MattStudley.BackEnd.Logging
{
    public class CachedLog
    {
        public LogLevel Level {get;set;}
        public string Message {get;set;}
        public Exception Exception {get;set;}
        public EventId EventId {get;set;}
    }
    
}