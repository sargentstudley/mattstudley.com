using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace MattStudley.BackEnd.Logging
{
    public interface ILogSession<TEntity> where TEntity: class
    {
        List<CachedLog> SessionLogs {get;set;}
        void LogDebug(EventId eventId, Exception ex, string message);
        void LogDebug(CachedLog log);
        void LogInformation(EventId eventId, Exception ex, string message);
        void LogInformation(CachedLog log);
        void LogWarning(EventId eventId, Exception ex, string message);
        void LogWarning(CachedLog log);
        void LogError(EventId eventId, Exception ex, string message);
        void LogError(CachedLog log);
    }
}