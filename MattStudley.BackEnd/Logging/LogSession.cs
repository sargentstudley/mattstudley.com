using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace MattStudley.BackEnd.Logging
{
    public class LogSession<TEntity> : ILogSession<TEntity> where TEntity : class
    {
        private ILogger _logger;

        public List<CachedLog> SessionLogs {get; set;}
        public LogSession(ILogger<TEntity> logger)
        {
            SessionLogs = new List<CachedLog>();
            _logger = logger;
        }

        public void LogDebug(EventId eventId, Exception ex, string message)
        {
            CachedLog log = new CachedLog() {
                EventId = eventId,
                Exception = ex,
                Message = message
            };

            LogDebug(log);
        }
        public void LogDebug(CachedLog log)
        {
            SessionLogs.Add(log);
            _logger.LogDebug(log.EventId,log.Exception,log.Message);
        }

         ///Enters a log, also caching it if needed. 
        public void LogInformation(EventId eventId, Exception ex, string message)
        {
            CachedLog log = new CachedLog() {
                EventId = eventId,
                Exception = ex,
                Message = message
            };

            LogInformation(log);
        }
        public void LogInformation(CachedLog log)
        {
            SessionLogs.Add(log);
            _logger.LogInformation(log.EventId,log.Exception,log.Message);
        }

        public void LogWarning(EventId eventId, Exception ex, string message)
        {
            CachedLog log = new CachedLog() {
                EventId = eventId,
                Exception = ex,
                Message = message
            };

            LogWarning(log);
        }
        public void LogWarning(CachedLog log)
        {
            SessionLogs.Add(log);
            _logger.LogWarning(log.EventId,log.Exception,log.Message);
        }

        public void LogError(EventId eventId, Exception ex, string message)
        {
            CachedLog log = new CachedLog() {
                EventId = eventId,
                Exception = ex,
                Message = message
            };

            LogError(log);
        }
        public void LogError(CachedLog log)
        {
            SessionLogs.Add(log);
            _logger.LogError(log.EventId,log.Exception,log.Message);
        }
    }
}