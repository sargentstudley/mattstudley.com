using MattStudley.BackEnd.Models;
using MattStudley.BackEnd.DAL.Repositories;

namespace MattStudley.BackEnd.DAL.UnitOfWork
{
    public interface ISkillsUnitOfWork
    {
        GenericRepository<SkillCategory> SkillCategoryRepository {get;}
        GenericRepository<Skill> SkillRepository {get;}

        bool Save();

    }
}