using System;
using Microsoft.EntityFrameworkCore;
using MattStudley.BackEnd.Models;
using MattStudley.BackEnd.DAL.Contexts;
using MattStudley.BackEnd.DAL.Repositories;
using MattStudley.BackEnd.Logging;


namespace MattStudley.BackEnd.DAL.UnitOfWork
{
    public class SkillsUnitOfWork : ISkillsUnitOfWork, IDisposable
    {
        private MattStudleyContext context;
        private GenericRepository<SkillCategory> skillCategoryRepository;
        private GenericRepository<Skill> skillRepository;

        private ILogSession<SkillsUnitOfWork> logSession;

        public SkillsUnitOfWork(MattStudleyContext dbContext, ILogSession<SkillsUnitOfWork> logger)
        {
            logSession = logger;
            context = dbContext;
        }

        public GenericRepository<SkillCategory> SkillCategoryRepository
        {
            get
            {

                if (this.skillCategoryRepository == null)
                {
                    this.skillCategoryRepository = new GenericRepository<SkillCategory>(context);
                }
                return skillCategoryRepository;
            }
        }

        public GenericRepository<Skill> SkillRepository
        {
            get
            {

                if (this.skillRepository == null)
                {
                    this.skillRepository = new GenericRepository<Skill>(context);
                }
                return skillRepository;
            }
        }

        public bool Save()
        { 
            try
            {
                context.SaveChanges();
                return true;
            }
            catch (DbUpdateException updateEx)
            {
                logSession.LogError(4001,updateEx,string.Format("Error updating Skills or Skill Category data: {0}",updateEx.Message));
            }
            catch (Exception ex)
            {
                logSession.LogError(4001,ex,string.Format("Could not save changes to Skills or Skill Category: {0}",ex.Message));
            }

            return false;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}