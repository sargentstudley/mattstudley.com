using Microsoft.EntityFrameworkCore;
using MattStudley.BackEnd.Models;

namespace MattStudley.BackEnd.DAL.Contexts
{
    public class MattStudleyContext : DbContext
    {
        public MattStudleyContext(DbContextOptions<MattStudleyContext> options) : base(options)
        {

        }
        public DbSet<SkillCategory> SkillCategorys { get; set; }
        public DbSet<Skill> Skills { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configure Primary keys for SkillCategory
            modelBuilder.Entity<SkillCategory>().HasKey(c => c.SkillCategoryId);
            modelBuilder.Entity<SkillCategory>().Property(c => c.SkillCategoryId).ValueGeneratedOnAdd();
            //Configure Primary keys for Skill
            modelBuilder.Entity<Skill>().HasKey(s => s.SkillId);
            modelBuilder.Entity<Skill>().Property(s => s.SkillId).ValueGeneratedOnAdd();
            modelBuilder.Entity<Skill>().Property(s =>s.TrainingNow).HasDefaultValue(false);
        }
    }
}